/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package Logica;
import Persistencia.ConexionDB;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class Producto {
    private String nombre;
    private String id;
    private double temperatura;
    private double valorBase;

    public Producto(String nombre, String id, double temperatura, double valorBase) {
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public Producto() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "nombre=" + nombre + ", id=" + id + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }

    // C R U D 
    // Read - Consultar en la base de datos los productos y enlistar
    // throws se utiliza para lanzar explicitamente una excepción
    public List<Producto> listarProductos() {
        List<Producto> listaProductos = new ArrayList<>(); // Instanciar una lista

        String sql = "SELECT * FROM Productos;";
        try {
            // ResultSet - Objeto que proporciona varios métodos para obtener
            // los datos de una columna correspondiente a una fila
            ConexionDB conexion = new ConexionDB(); // Objeto de la clase conexion DB
            ResultSet rs = conexion.consultarDB(sql);
            
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setId(rs.getString("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorBase"));
                listaProductos.add(p); // Aquí enlistamos todos los datos traidos de la BD
            }
        } catch (Exception ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        }
        return listaProductos;
    }

    // Crear
    public boolean guardarProducto() {
        try {
            ConexionDB conexion = new ConexionDB(); // objeto

            String sql = "INSERT INTO Productos(id, nombre, temperatura, valorBase)"
                    + "VALUES('" + this.id + "','" + this.nombre + "'," + this.temperatura + "," + this.valorBase + ");";
            if (conexion.setAutoCommitDB(false)) {//Para que la db no confirme automaticamente el cambio
                if (conexion.insertarDB(sql)) {
                    conexion.commitDB();//confirma el cambio a la BD
                    conexion.cerrarConexion();
                    return true;
                } else {
                    conexion.rollbackDB();
                    conexion.cerrarConexion();
                    return false;
                }
            } else {
                conexion.cerrarConexion();
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Error al guardar producto:" + ex.getMessage());

        }
        return false;
    }

    // updata
    public boolean actualizarProducto() {
        try {
            ConexionDB conexion = new ConexionDB(); // Objeto conexion

            String sql = "UPDATE Productos SET id='"
                    + this.id + "',nombre='" + this.nombre
                    + "',temperatura=" + this.temperatura + ",valorBase="
                    + this.valorBase + " WHERE id='" + this.id + "';";
            if (conexion.setAutoCommitDB(false)) {
                if (conexion.actualizarDB(sql)) {
                    conexion.commitDB();
                    conexion.cerrarConexion();
                    return true;
                } else {
                    conexion.rollbackDB();
                    conexion.cerrarConexion();
                    return false;
                }
            } else {
                conexion.cerrarConexion();
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Error al actualizar el producto:" + ex.getMessage());
        }
        return false;

    }

    // Delete
    public boolean eliminarProducto() {
        try {
            ConexionDB conexion = new ConexionDB();

            String sql = "DELETE FROM Productos WHERE id='" + this.id + "';";
            if (conexion.setAutoCommitDB(false)) {
                if (conexion.actualizarDB(sql)) {
                    conexion.commitDB();
                    conexion.cerrarConexion();
                    return true;
                } else {
                    conexion.rollbackDB();
                    conexion.cerrarConexion();
                    return false;
                }
            } else {
                conexion.cerrarConexion();
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Error al actualizar el producto:" + ex.getMessage());
        }
        return false;
    }
    public double calcularCostoDeAlmacenamiento() {
        double costo;
        if (this.temperatura >= 21) {
            costo = getValorBase() * 1.10;
        } else {
            costo = getValorBase() * 1.20;
        }
        return costo;
    }

}

